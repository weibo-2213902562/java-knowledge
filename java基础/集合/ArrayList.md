# # ArrayList笔记（JDK1.8）

## #1）ArrayList是什么？

ArrayList是一个普通类，类似一个可动态调整大小的数组；它实现了List接口。

1）动态数组结构

2）扩容，长度默认1.5倍（增加所需长度比1.5倍大的话，直接用所需长度）



## #2）源码分析

### #2.1）成员变量

```java
//默认长度，10个
private static final int DEFAULT_CAPACITY = 10;

//空数组
private static final Object[] EMPTY_ELEMENTDATA = {};

//空数组
private static final Object[] DEFAULTCAPACITY_EMPTY_ELEMENTDATA = {};

//存放实际数据的数组
transient Object[] elementData;

//数据个数，逻辑个数，初始化时为0
private int size;
```



### #2.2）初始化方法

```java
public ArrayList(int initialCapacity) {//指定长度
    if (initialCapacity > 0) {//长度大于0，直接创建该长度大小的数组
      this.elementData = new Object[initialCapacity];
    } else if (initialCapacity == 0) {//长度为0，使用默认静态空数组
      this.elementData = EMPTY_ELEMENTDATA;
    } else {//长度为负数，直接报错
      throw new IllegalArgumentException("Illegal Capacity: "+ initialCapacity);
    }
}

public ArrayList() {//不指定长度，使用默认静态空数组
  	this.elementData = DEFAULTCAPACITY_EMPTY_ELEMENTDATA;
}

public ArrayList(Collection<? extends E> c) {//通过集合创建
    elementData = c.toArray();//将集合转为数组
    if ((size = elementData.length) != 0) {//集合元素不为空时
      if (elementData.getClass() != Object[].class)//集合有可能不是Object类型，因此需要转换
        elementData = Arrays.copyOf(elementData, size, Object[].class);
    } else {//集合元素为空时，使用默认静态数组
      this.elementData = EMPTY_ELEMENTDATA;
    }
}
```

ps：若是一开始，我们知道需要创建的长度很大，那么就直接指定长度吧，这样会节省ArrayList对于底层数组创建和赋值等一系列的开销。



### #2.3）常用接口（增删改查）

#### ArrayList增加单个元素接口：方式有，追加和指定序号插入

方式1：追加元素，大概流程：是否数组需要扩容-->扩容（可能）-->赋值

```java
public boolean add(E e) {    
    ensureCapacityInternal(size + 1);//这里判断数组是否需要扩容，若需要直接扩容
    elementData[size++] = e;//追加元素，并且将逻辑长度大小+1
    return true;
}

private void ensureCapacityInternal(int minCapacity) {
    if (elementData == DEFAULTCAPACITY_EMPTY_ELEMENTDATA) {
    	minCapacity = Math.max(DEFAULT_CAPACITY, minCapacity);
    }

    ensureExplicitCapacity(minCapacity);
}


private void ensureExplicitCapacity(int minCapacity) {
    modCount++;//这是父类AbstractList变量，用于纪录修改次数

    if (minCapacity - elementData.length > 0)//如果数组所需最小长度>目前数组实际长度，那么就需要扩容
    	grow(minCapacity);//扩容
}

private void grow(int minCapacity) {//数组扩容
    int oldCapacity = elementData.length;
    //这里做了位运算，效率高一点
    int newCapacity = oldCapacity + (oldCapacity >> 1);//扩容规则：数组长度是原来的1.5倍
    if (newCapacity - minCapacity < 0)//如果新的长度<所需长度，那么用所需长度，作为新的数组长度
      newCapacity = minCapacity;
    if (newCapacity - MAX_ARRAY_SIZE > 0)//数组长度不能超过Integer范围
      newCapacity = hugeCapacity(minCapacity);
    
  	//然后通过Arrays工具类，对数组进行扩容，其实这里会重新创建新的数组
    elementData = Arrays.copyOf(elementData, newCapacity);//底层调用本地方法（System.arraycopy）
}

private static final int MAX_ARRAY_SIZE = Integer.MAX_VALUE - 8;

private static int hugeCapacity(int minCapacity) {
    if (minCapacity < 0) //如果为负数，证明溢出了
      throw new OutOfMemoryError();
    //返回不超过Integer返回的值
    return (minCapacity > MAX_ARRAY_SIZE) ? Integer.MAX_VALUE : MAX_ARRAY_SIZE;
}
```


方式2：指定序号插入元素

```java
public void add(int index, E element) {
    rangeCheckForAdd(index);

  	//这里其实跟方式1很像
    ensureCapacityInternal(size + 1);//判断是否扩容，需要就扩
    System.arraycopy(elementData, index, elementData, index + 1, size - index);//数组复制
    elementData[index] = element;//赋值
    size++;//逻辑长度+1
}

private void rangeCheckForAdd(int index) {//判断是否坐标越界
    if (index > size || index < 0)
      throw new IndexOutOfBoundsException(outOfBoundsMsg(index));
}
```



#### ArrayList删除元素接口：方式有，指定序号删除，指定对象删除，全删

方式1：指定序号删除

```java
public E remove(int index) {
    rangeCheck(index);//检查

    modCount++;//这是父类AbstractList变量，用于纪录修改次数
    E oldValue = elementData(index);//返回删除的对象

    int numMoved = size - index - 1;//计算需要移动的元素个数
    if (numMoved > 0)//数组元素移动（创建新数组，并对数组赋值）
    	System.arraycopy(elementData, index+1, elementData, index, numMoved);
  	
    elementData[--size] = null; //逻辑长度-1，并且将最后一个数组元素设置为null

    return oldValue;
}

private void rangeCheck(int index) {//检查坐标，是否在范围内
    if (index >= size)
      throw new IndexOutOfBoundsException(outOfBoundsMsg(index));
}

private String outOfBoundsMsg(int index) {
  	return "Index: "+index+", Size: "+size;
}

@SuppressWarnings("unchecked")
E elementData(int index) {//获取对应坐标的对象
	return (E) elementData[index];
}
```



方式2：指定对象删除

```java
public boolean remove(Object o) {//无非就是遍历对比，找到对应的index，然后在进行删除，后续的逻辑跟方式1基本一样
    if (o == null) {
      	for (int index = 0; index < size; index++)
        	if (elementData[index] == null) {
      			fastRemove(index);
      			return true;
    		}
    } else {
      	for (int index = 0; index < size; index++)
        	if (o.equals(elementData[index])) {//这里注意，如果equals是默认Object的话，对比的是地址
                fastRemove(index);
                return true;
        	}
    }
    return false;
}

//这里的操作跟方式1类似，因为已经找到对应的index了
private void fastRemove(int index) {
    modCount++;
    int numMoved = size - index - 1;
    if (numMoved > 0)
      System.arraycopy(elementData, index+1, elementData, index, numMoved);
    elementData[--size] = null;
}
```



方式3：全删

```java
public void clear() {//遍历全删
    modCount++;//修改次数+1

    for (int i = 0; i < size; i++)//所有对象设置为Null，让GC自动回收堆中对象
      elementData[i] = null;

    size = 0;//逻辑个数重置为0
}
```



#### ArrayList修改元素接口：方式有，指定序号修改

方式1：指定序号修改

```java
public E set(int index, E element) {
    rangeCheck(index);//判断是否出界

    E oldValue = elementData(index);//直接查出对象返回
    elementData[index] = element;//修改坐标对象引用
    return oldValue;
}

@SuppressWarnings("unchecked")
E elementData(int index) {
	return (E) elementData[index];
}
```



#### ArrayList查询元素接口：方式有，指定序号查询

方式1：指定序号查询

```java
public E get(int index) {
    rangeCheck(index);//判断是否出界

    return elementData(index);//直接查出对象返回
}

@SuppressWarnings("unchecked")
E elementData(int index) {
	return (E) elementData[index];
}
```



### #2.4）其他接口

#### 获取ArrayList的逻辑个数

```java
public int size() {
	return size;//这里直接就返回成员变量size了，因为每次修改，都会对元素size做纪录
}
```



### #2.5）时间复杂度

### 查询复杂度：(假设底层数组长度为n)

若是直接通过index查询：O（1）

若是需要循环遍历查询：O（n）