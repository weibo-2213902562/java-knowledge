# #LinkedList笔记(JDK1.8)

## #1）LinkedList是什么？

双向链表结构



## #2）源码分析

### 2.1）成员变量

```java
transient int size = 0;//纪录节点逻辑个数

transient Node<E> first;//纪录头节点

transient Node<E> last;//纪录尾节点
```



### 2.2）内部类（双向链表结构的节点实现类）

```java
//节点类
private static class Node<E> {
    E item;//当前节点，内容对象
    Node<E> next;//下一个节点，如果当前节点是尾节点，那么next为null
    Node<E> prev;//上一个节点，如果当前节点是头节点，那么prev为null

    Node(Node<E> prev, E element, Node<E> next) {//构造方法为（前节点，自身对象，后节点）
        this.item = element;
        this.next = next;
        this.prev = prev;
    }
}
```



### 2.3）初始化方法

```java
public LinkedList() {

}

public LinkedList(Collection<? extends E> c) {
    this();
    addAll(c);//将集合作为内容参数添加到链表中
}

public boolean addAll(Collection<? extends E> c) {
  	return addAll(size, c);
}

public boolean addAll(int index, Collection<? extends E> c) {
    checkPositionIndex(index);//检查index是否越界了

    Object[] a = c.toArray();//AbstractCollection父类方法，集合转数组
    int numNew = a.length;
    if (numNew == 0)//如果没有元素，那么直接返回
      return false;

    Node<E> pred, succ;//纪录上一个节点和下一个节点
    if (index == size) {//在最后添加（追加），所以
      succ = null;//下一个节点为null
      pred = last;//上一个节点为原来的尾节点
    } else {//否则，就是在已有节点插入
      succ = node(index);//找出原index节点，作为下一个节点
      pred = succ.prev;//找出原index节点的上一个节点，作为上一个节点
    }

    for (Object o : a) {//遍历新增元素，并添加到链表当中
      @SuppressWarnings("unchecked") E e = (E) o;
      Node<E> newNode = new Node<>(pred, e, null);
      if (pred == null)//如果上一个节点是空，那么第一个添加的作为头节点
        first = newNode;
      else//如果上一个节点不为空，那么让它的next指向当前添加的节点，作为绑定关联
        pred.next = newNode;
      pred = newNode;//刷新上一个节点纪录
    }

    if (succ == null) {//如果一开始的尾节点为空，那么插入在最后尾巴，所以需要将上一个节点的最后一次纪录设置为尾节点
      last = pred;
    } else {//如果一开始的尾节点不为空，那么让尾节点的pre指向，上一个节点的最后一次纪录，并且让上一个节点的最后一次纪录的next指向该尾节点，实现互相关联
      pred.next = succ;
      succ.prev = pred;
    }

    size += numNew;//刷新逻辑个数
    modCount++;//刷新修改册数
    return true;
}

//检查是否在范围内并提示
private void checkPositionIndex(int index) {
  	if (!isPositionIndex(index))
    	throw new IndexOutOfBoundsException(outOfBoundsMsg(index));
}

//检查是否在范围内
private boolean isPositionIndex(int index) {
  	return index >= 0 && index <= size;
}

//越界提示
private String outOfBoundsMsg(int index) {
  	return "Index: "+index+", Size: "+size;
}

//通过index查询对应的节点
Node<E> node(int index) {
  	//这里将链表切成2份来操作
    if (index < (size >> 1)) {//如果index是小于二分之一的size，那么就从头节点往后查
      Node<E> x = first;
      for (int i = 0; i < index; i++)
        x = x.next;
      return x;
    } else {//否则，就是从尾节点往前面查询
      Node<E> x = last;
      for (int i = size - 1; i > index; i--)
        x = x.prev;
      return x;
    }
}
```

总结：带有集合作为参数的初始化方法，主要实现的逻辑为：

1）检查传入的index是否有越界（初始化时用的index是size，所以不存在越界一说）；

2）检查传入的集合，是否存在元素；

3）查询原index下的节点元素，并纪录它的上一个节点和下一个节点；

4）遍历需要插入的元素，每次都让上一个节点指向插入的后一个节点，循环后，形成一个链表；

5）处理尾节点问题，关联上一开始断开的尾节点；

6）刷新逻辑个数和修改次数。



### 2.4）常用接口（增删改查）

#### 2.4.1）增加节点

方式1）LinkedList增加元素（单个）接口：

```java
//添加元素对象
public boolean add(E e) {
    linkLast(e);
    return true;
}

//头插
public void addFirst(E e) {
  	linkFirst(e);
}

//尾插
public void addLast(E e) {
  	linkLast(e);
}

//在特定index上添加
public void add(int index, E element) {
    checkPositionIndex(index);//检查是否在范围内并提示，这里前面提过就不再写了

    if (index == size)//如果序号是最后一个位置，那么插入到最后尾部
      linkLast(element);
    else//否则，插入到对应的index位置
      linkBefore(element, node(index));
}

//头插逻辑
private void linkFirst(E e) {
    final Node<E> f = first;//first是成员变量，用于纪录头节点
    final Node<E> newNode = new Node<>(null, e, f);//创建新的头节点
    first = newNode;//刷新头节点指向
    if (f == null)//如果原头节点为空，那么原链表没有元素，尾节点也是新插入的节点
      last = newNode;
    else//否则，原头节点的prev指向新建的头节点
      f.prev = newNode;
    size++;//逻辑个数+1
    modCount++;//修改个数+1
}

//尾插逻辑
void linkLast(E e) {
    final Node<E> l = last;//last是成员变量，用于纪录尾节点
    final Node<E> newNode = new Node<>(l, e, null);//新建节点，并且关联尾节点
    last = newNode;//刷新尾节点
    if (l == null)//如果原尾节点为空，那么原链表没有元素，头节点也是新插入的节点
      first = newNode;
    else//否则，原尾节点的next指向新插入的节点
      l.next = newNode;
    size++;//逻辑个数+1
    modCount++;//修改次数+1
}

//在节点B前插入
void linkBefore(E e, Node<E> succ) {
    final Node<E> pred = succ.prev;//纪录节点B的前一个节点A
    final Node<E> newNode = new Node<>(pred, e, succ);//创建AB节点之间的新节点X
    succ.prev = newNode;//修改节点B的prev为新节点X
    if (pred == null)//如果节点A原本是Null，那么证明在头节点前插入，刷新first纪录
      first = newNode;
    else//否则，让节点A的next指向新节点X
      pred.next = newNode;
    size++;//逻辑个数+1
    modCount++;//修改次数+1
}

```

总结：

1）插入的方式，主要是头插，尾插，或者指定index前插入；

2）无论那种插入，基本上实现的逻辑是，新建的节点（指向原前节点和原后节点），然后再将原前节点的next指向新节点，后节点的prev指向新节点；

3）根据具体的位置，若有头或尾，需要刷新first或last纪录；

4）刷新逻辑个数和修改个数。



方式2）LinkedList增加元素（多个）接口：集合方式，初始化方法哪里有说

```java
public boolean addAll(Collection<? extends E> c);

public boolean addAll(int index, Collection<? extends E> c);
```



#### 2.4.2）删除节点

方式1）删除单个节点

```java
//删除头节点
public E removeFirst() {//有头节点，就解绑，没有就抛异常
    final Node<E> f = first;
    if (f == null)
      throw new NoSuchElementException();
    return unlinkFirst(f);//主要解绑逻辑
}

//对头节点解链
private E unlinkFirst(Node<E> f) {
    final E element = f.item;
    final Node<E> next = f.next;//获取头节点的下一个节点
  	//将头节点的对象引用和next引用设置为空，以便GC回收
    f.item = null;
    f.next = null;
    first = next;//将头节点的下一个节点，设置为first新的头节点
    if (next == null)//若是next为空，即只有一个节点
      last = null;//需要将尾节点也设置为空
    else//否则，将新的头节点的prev设置为空，因为头节点没有前一个节点
      next.prev = null;
    size--;//逻辑个数-1
    modCount++;//修改次数+1
    return element;
}

//删除尾节点
public E removeLast() {//有尾节点，就解绑，没有就抛异常
    final Node<E> l = last;
    if (l == null)
      throw new NoSuchElementException();
    return unlinkLast(l);//主要解绑逻辑
}

//对尾节点解链
private E unlinkLast(Node<E> l) {
    final E element = l.item;
    final Node<E> prev = l.prev;//纪录尾节点的上一个节点
  	//将尾节点的引用设置为空，以便GC回收
    l.item = null;
    l.prev = null;
    last = prev;//刷新尾节点指向
    if (prev == null)//如果上一个节点为空，那么链表中只有一个节点，需要将头节点也设置为空
      first = null;
    else//否则，让上一个节点的next指向为空
      prev.next = null;
    size--;//逻辑个数-1
    modCount++;//修改次数+1
    return element;
}

//删除指定位置
public E remove(int index) {
    checkElementIndex(index);
    return unlink(node(index));
}

//删除指定对象
public boolean remove(Object o) {
    if (o == null) {//如果对象为空，那么遍历节点，查找节点内容对象为null的节点
      for (Node<E> x = first; x != null; x = x.next) {
        if (x.item == null) {
          unlink(x);//解绑它
          return true;
        }
      }
    } else {//如果对象不为空，那么遍历，并通过Equals方法，对比找出后删除，这里注意，如果没有复写equals方法，那么就是跟Object.equals一样，对比的是对象地址
      for (Node<E> x = first; x != null; x = x.next) {
        if (o.equals(x.item)) {
          unlink(x);
          return true;
        }
      }
    }
    return false;
}

//对节点解链
E unlink(Node<E> x) {
    final E element = x.item;
    final Node<E> next = x.next;
    final Node<E> prev = x.prev;

    if (prev == null) {//如果上一个节点为空，即头节点，因此需要刷新头节点为下一个节点
      first = next;
    } else {//否则，将上一个节点的next直接指向下一个节点，不再指向当前节点，并且当前节点的prev引用断开
      prev.next = next;
      x.prev = null;
    }

    if (next == null) {//如果下一个节点为空，即尾节点，因此需要刷新尾节点为上一个节点
      last = prev;
    } else {//否则，将下一个节点的prev直接指向上一个节点，不再指向当前节点，并且当前节点的next引用断开
      next.prev = prev;
      x.next = null;
    }

    x.item = null;//将节点指向的内容对象引用设置为空
    size--;//逻辑个数-1
    modCount++;//修改次数+1
    return element;
}
```

总结：删除单个节点

1）通过index或者对象对比的方式，找到对应的节点A；

2）通过节点A，找到前后节点，然后断开A的对前后节点的连接；

3）前后节点互相连接；

4）如果节点是临界节点（头尾节点），那么需要刷新头尾节点引用；

5）修改逻辑个数和修改次数；



方式2）删除所有节点

```java
public void clear() {
    // Clearing all of the links between nodes is "unnecessary", but:
    // - helps a generational GC if the discarded nodes inhabit
    //   more than one generation
    // - is sure to free memory even if there is a reachable Iterator
    for (Node<E> x = first; x != null; ) {//从头节点开始，遍历，然后将所有节点指向和对象设置为空
      Node<E> next = x.next;
      x.item = null;
      x.next = null;
      x.prev = null;
      x = next;
    }
    first = last = null;//头尾节点设置为空
    size = 0;//逻辑个数设置为0
    modCount++;//修改次数+1
}
```

总结：删除所有节点逻辑

1）从链表结构的头节点，遍历下去，将所有节点的指向设置为空；

2）头尾节点设置为空；

3）刷新逻辑个数和修改次数。



#### 2.4.3）查询节点

```java
//直接返回
public E getFirst() {
    final Node<E> f = first;
    if (f == null)
      throw new NoSuchElementException();
    return f.item;
}

//直接返回
public E getLast() {
    final Node<E> l = last;
    if (l == null)
      throw new NoSuchElementException();
    return l.item;
}

//通过index查找
public E get(int index) {
    checkElementIndex(index);
    return node(index).item;//上面删除接口部分提及过
}
```



#### 2.4.4）修改节点对象内容

```java
//修改对应index下的内容对象
public E set(int index, E element) {
    checkElementIndex(index);//检查是否越界
    Node<E> x = node(index);//获取到对应index下的节点
    E oldVal = x.item;
    x.item = element;
    return oldVal;
}
```

总结：找到对应index节点，然后修改节点对象引用。



### 3）其他方法

#### 3.1）查询元素个数

```java
public int size() {
  	return size;
}
```



#### 3.2）查询元素index

```java
//从头节点开始，查询对象在链表中的第一个index位置，不存在返回-1
public int indexOf(Object o) {
    int index = 0;
    if (o == null) {
      for (Node<E> x = first; x != null; x = x.next) {
        if (x.item == null)
          return index;
        index++;
      }
    } else {
      for (Node<E> x = first; x != null; x = x.next) {
        if (o.equals(x.item))
          return index;
        index++;
      }
    }
    return -1;
}

//从尾节点开始，查询对象在链表中的第一个index位置，不存在返回-1
public int lastIndexOf(Object o) {
    int index = size;
    if (o == null) {
      for (Node<E> x = last; x != null; x = x.prev) {
        index--;
        if (x.item == null)
          return index;
      }
    } else {
      for (Node<E> x = last; x != null; x = x.prev) {
        index--;
        if (o.equals(x.item))
          return index;
      }
    }
    return -1;
}
```



#### 3.3）检查是否包含（接口使用了上述的：查询元素index接口）

```java
public boolean contains(Object o) {
  	return indexOf(o) != -1;
}
```

